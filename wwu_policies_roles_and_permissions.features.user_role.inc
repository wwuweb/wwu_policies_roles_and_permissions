<?php
/**
 * @file
 * wwu_policies_roles_and_permissions.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function wwu_policies_roles_and_permissions_user_default_roles() {
  $roles = array();

  // Exported role: policy author.
  $roles['policy author'] = array(
    'name' => 'policy author',
    'weight' => 3,
  );

  // Exported role: policy editor.
  $roles['policy editor'] = array(
    'name' => 'policy editor',
    'weight' => 4,
  );

  // Exported role: policy reviewer.
  $roles['policy reviewer'] = array(
    'name' => 'policy reviewer',
    'weight' => 2,
  );

  return $roles;
}
