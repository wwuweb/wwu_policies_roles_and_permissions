<?php
/**
 * @file
 * wwu_policies_roles_and_permissions.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function wwu_policies_roles_and_permissions_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'access administration menu'.
  $permissions['access administration menu'] = array(
    'name' => 'access administration menu',
    'roles' => array(
      'administrator' => 'administrator',
      'policy author' => 'policy author',
      'policy editor' => 'policy editor',
      'policy reviewer' => 'policy reviewer',
    ),
    'module' => 'admin_menu',
  );

  // Exported permission: 'access administration pages'.
  $permissions['access administration pages'] = array(
    'name' => 'access administration pages',
    'roles' => array(
      'administrator' => 'administrator',
      'policy author' => 'policy author',
      'policy editor' => 'policy editor',
      'policy reviewer' => 'policy reviewer',
    ),
    'module' => 'system',
  );

  // Exported permission: 'access comments'.
  $permissions['access comments'] = array(
    'name' => 'access comments',
    'roles' => array(
      'administrator' => 'administrator',
      'policy author' => 'policy author',
      'policy editor' => 'policy editor',
      'policy reviewer' => 'policy reviewer',
    ),
    'module' => 'comment',
  );

  // Exported permission: 'access content'.
  $permissions['access content'] = array(
    'name' => 'access content',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: 'access content overview'.
  $permissions['access content overview'] = array(
    'name' => 'access content overview',
    'roles' => array(
      'administrator' => 'administrator',
      'policy editor' => 'policy editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'access private fields'.
  $permissions['access private fields'] = array(
    'name' => 'access private fields',
    'roles' => array(
      'administrator' => 'administrator',
      'policy editor' => 'policy editor',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'access site reports'.
  $permissions['access site reports'] = array(
    'name' => 'access site reports',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'system',
  );

  // Exported permission: 'access workbench'.
  $permissions['access workbench'] = array(
    'name' => 'access workbench',
    'roles' => array(
      'administrator' => 'administrator',
      'policy author' => 'policy author',
      'policy editor' => 'policy editor',
      'policy reviewer' => 'policy reviewer',
    ),
    'module' => 'workbench',
  );

  // Exported permission: 'administer content types'.
  $permissions['administer content types'] = array(
    'name' => 'administer content types',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'administer nodes'.
  $permissions['administer nodes'] = array(
    'name' => 'administer nodes',
    'roles' => array(
      'administrator' => 'administrator',
      'policy editor' => 'policy editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'administer taxonomy'.
  $permissions['administer taxonomy'] = array(
    'name' => 'administer taxonomy',
    'roles' => array(
      'administrator' => 'administrator',
      'policy editor' => 'policy editor',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'administer workbench'.
  $permissions['administer workbench'] = array(
    'name' => 'administer workbench',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'workbench',
  );

  // Exported permission: 'administer workbench moderation'.
  $permissions['administer workbench moderation'] = array(
    'name' => 'administer workbench moderation',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'workbench_moderation',
  );

  // Exported permission: 'bypass node access'.
  $permissions['bypass node access'] = array(
    'name' => 'bypass node access',
    'roles' => array(
      'administrator' => 'administrator',
      'policy editor' => 'policy editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'bypass workbench moderation'.
  $permissions['bypass workbench moderation'] = array(
    'name' => 'bypass workbench moderation',
    'roles' => array(
      'administrator' => 'administrator',
      'policy editor' => 'policy editor',
    ),
    'module' => 'workbench_moderation',
  );

  // Exported permission: 'create authority content'.
  $permissions['create authority content'] = array(
    'name' => 'create authority content',
    'roles' => array(
      'administrator' => 'administrator',
      'policy author' => 'policy author',
      'policy editor' => 'policy editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'create form content'.
  $permissions['create form content'] = array(
    'name' => 'create form content',
    'roles' => array(
      'policy editor' => 'policy editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'create policy content'.
  $permissions['create policy content'] = array(
    'name' => 'create policy content',
    'roles' => array(
      'administrator' => 'administrator',
      'policy author' => 'policy author',
      'policy editor' => 'policy editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'create procedure content'.
  $permissions['create procedure content'] = array(
    'name' => 'create procedure content',
    'roles' => array(
      'policy editor' => 'policy editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'create reference content'.
  $permissions['create reference content'] = array(
    'name' => 'create reference content',
    'roles' => array(
      'administrator' => 'administrator',
      'policy author' => 'policy author',
      'policy editor' => 'policy editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any authority content'.
  $permissions['delete any authority content'] = array(
    'name' => 'delete any authority content',
    'roles' => array(
      'administrator' => 'administrator',
      'policy editor' => 'policy editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any form content'.
  $permissions['delete any form content'] = array(
    'name' => 'delete any form content',
    'roles' => array(
      'policy editor' => 'policy editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any policy content'.
  $permissions['delete any policy content'] = array(
    'name' => 'delete any policy content',
    'roles' => array(
      'administrator' => 'administrator',
      'policy editor' => 'policy editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any procedure content'.
  $permissions['delete any procedure content'] = array(
    'name' => 'delete any procedure content',
    'roles' => array(
      'policy editor' => 'policy editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any reference content'.
  $permissions['delete any reference content'] = array(
    'name' => 'delete any reference content',
    'roles' => array(
      'administrator' => 'administrator',
      'policy editor' => 'policy editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own authority content'.
  $permissions['delete own authority content'] = array(
    'name' => 'delete own authority content',
    'roles' => array(
      'administrator' => 'administrator',
      'policy editor' => 'policy editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own form content'.
  $permissions['delete own form content'] = array(
    'name' => 'delete own form content',
    'roles' => array(
      'policy editor' => 'policy editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own policy content'.
  $permissions['delete own policy content'] = array(
    'name' => 'delete own policy content',
    'roles' => array(
      'administrator' => 'administrator',
      'policy editor' => 'policy editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own procedure content'.
  $permissions['delete own procedure content'] = array(
    'name' => 'delete own procedure content',
    'roles' => array(
      'policy editor' => 'policy editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own reference content'.
  $permissions['delete own reference content'] = array(
    'name' => 'delete own reference content',
    'roles' => array(
      'administrator' => 'administrator',
      'policy editor' => 'policy editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete revisions'.
  $permissions['delete revisions'] = array(
    'name' => 'delete revisions',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any authority content'.
  $permissions['edit any authority content'] = array(
    'name' => 'edit any authority content',
    'roles' => array(
      'administrator' => 'administrator',
      'policy editor' => 'policy editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any form content'.
  $permissions['edit any form content'] = array(
    'name' => 'edit any form content',
    'roles' => array(
      'policy editor' => 'policy editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any policy content'.
  $permissions['edit any policy content'] = array(
    'name' => 'edit any policy content',
    'roles' => array(
      'administrator' => 'administrator',
      'policy editor' => 'policy editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any procedure content'.
  $permissions['edit any procedure content'] = array(
    'name' => 'edit any procedure content',
    'roles' => array(
      'policy editor' => 'policy editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any reference content'.
  $permissions['edit any reference content'] = array(
    'name' => 'edit any reference content',
    'roles' => array(
      'administrator' => 'administrator',
      'policy editor' => 'policy editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own authority content'.
  $permissions['edit own authority content'] = array(
    'name' => 'edit own authority content',
    'roles' => array(
      'administrator' => 'administrator',
      'policy author' => 'policy author',
      'policy editor' => 'policy editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own comments'.
  $permissions['edit own comments'] = array(
    'name' => 'edit own comments',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'comment',
  );

  // Exported permission: 'edit own form content'.
  $permissions['edit own form content'] = array(
    'name' => 'edit own form content',
    'roles' => array(
      'policy editor' => 'policy editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own policy content'.
  $permissions['edit own policy content'] = array(
    'name' => 'edit own policy content',
    'roles' => array(
      'administrator' => 'administrator',
      'policy author' => 'policy author',
      'policy editor' => 'policy editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own procedure content'.
  $permissions['edit own procedure content'] = array(
    'name' => 'edit own procedure content',
    'roles' => array(
      'policy editor' => 'policy editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own reference content'.
  $permissions['edit own reference content'] = array(
    'name' => 'edit own reference content',
    'roles' => array(
      'administrator' => 'administrator',
      'policy author' => 'policy author',
      'policy editor' => 'policy editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'moderate content from draft to needs_review'.
  $permissions['moderate content from draft to needs_review'] = array(
    'name' => 'moderate content from draft to needs_review',
    'roles' => array(
      'administrator' => 'administrator',
      'policy author' => 'policy author',
      'policy editor' => 'policy editor',
    ),
    'module' => 'workbench_moderation',
  );

  // Exported permission: 'moderate content from needs_review to draft'.
  $permissions['moderate content from needs_review to draft'] = array(
    'name' => 'moderate content from needs_review to draft',
    'roles' => array(
      'administrator' => 'administrator',
      'policy editor' => 'policy editor',
    ),
    'module' => 'workbench_moderation',
  );

  // Exported permission: 'moderate content from needs_review to published'.
  $permissions['moderate content from needs_review to published'] = array(
    'name' => 'moderate content from needs_review to published',
    'roles' => array(
      'administrator' => 'administrator',
      'policy editor' => 'policy editor',
    ),
    'module' => 'workbench_moderation',
  );

  // Exported permission: 'post comments'.
  $permissions['post comments'] = array(
    'name' => 'post comments',
    'roles' => array(
      'administrator' => 'administrator',
      'policy author' => 'policy author',
      'policy editor' => 'policy editor',
      'policy reviewer' => 'policy reviewer',
    ),
    'module' => 'comment',
  );

  // Exported permission: 'revert revisions'.
  $permissions['revert revisions'] = array(
    'name' => 'revert revisions',
    'roles' => array(
      'administrator' => 'administrator',
      'policy author' => 'policy author',
      'policy editor' => 'policy editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'skip comment approval'.
  $permissions['skip comment approval'] = array(
    'name' => 'skip comment approval',
    'roles' => array(
      'administrator' => 'administrator',
      'policy author' => 'policy author',
      'policy editor' => 'policy editor',
      'policy reviewer' => 'policy reviewer',
    ),
    'module' => 'comment',
  );

  // Exported permission: 'use text format clean_html'.
  $permissions['use text format clean_html'] = array(
    'name' => 'use text format clean_html',
    'roles' => array(
      'administrator' => 'administrator',
      'policy author' => 'policy author',
      'policy editor' => 'policy editor',
    ),
    'module' => 'filter',
  );

  // Exported permission: 'use workbench_moderation my drafts tab'.
  $permissions['use workbench_moderation my drafts tab'] = array(
    'name' => 'use workbench_moderation my drafts tab',
    'roles' => array(
      'administrator' => 'administrator',
      'policy author' => 'policy author',
      'policy editor' => 'policy editor',
    ),
    'module' => 'workbench_moderation',
  );

  // Exported permission: 'use workbench_moderation needs review tab'.
  $permissions['use workbench_moderation needs review tab'] = array(
    'name' => 'use workbench_moderation needs review tab',
    'roles' => array(
      'administrator' => 'administrator',
      'policy editor' => 'policy editor',
      'policy reviewer' => 'policy reviewer',
    ),
    'module' => 'workbench_moderation',
  );

  // Exported permission: 'view all unpublished content'.
  $permissions['view all unpublished content'] = array(
    'name' => 'view all unpublished content',
    'roles' => array(
      'administrator' => 'administrator',
      'policy editor' => 'policy editor',
    ),
    'module' => 'workbench_moderation',
  );

  // Exported permission: 'view moderation history'.
  $permissions['view moderation history'] = array(
    'name' => 'view moderation history',
    'roles' => array(
      'administrator' => 'administrator',
      'policy author' => 'policy author',
      'policy editor' => 'policy editor',
    ),
    'module' => 'workbench_moderation',
  );

  // Exported permission: 'view moderation messages'.
  $permissions['view moderation messages'] = array(
    'name' => 'view moderation messages',
    'roles' => array(
      'administrator' => 'administrator',
      'policy author' => 'policy author',
      'policy editor' => 'policy editor',
    ),
    'module' => 'workbench_moderation',
  );

  // Exported permission: 'view own unpublished content'.
  $permissions['view own unpublished content'] = array(
    'name' => 'view own unpublished content',
    'roles' => array(
      'administrator' => 'administrator',
      'policy author' => 'policy author',
      'policy editor' => 'policy editor',
      'policy reviewer' => 'policy reviewer',
    ),
    'module' => 'node',
  );

  // Exported permission: 'view revisions'.
  $permissions['view revisions'] = array(
    'name' => 'view revisions',
    'roles' => array(
      'administrator' => 'administrator',
      'policy author' => 'policy author',
      'policy editor' => 'policy editor',
      'policy reviewer' => 'policy reviewer',
    ),
    'module' => 'node',
  );

  // Exported permission: 'view the administration theme'.
  $permissions['view the administration theme'] = array(
    'name' => 'view the administration theme',
    'roles' => array(
      'administrator' => 'administrator',
      'policy author' => 'policy author',
      'policy editor' => 'policy editor',
      'policy reviewer' => 'policy reviewer',
    ),
    'module' => 'system',
  );

  return $permissions;
}
